const express = require('express');
const Track = require('../models/Track');

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		const query = {};

		if (req.query.album) {
			query.album = req.query.album;
		}

		const albums = await Track.find(query).populate('album', 'name');
		res.send(albums);
	} catch (e) {
		res.sendStatus(500);
	}
});

router.post('/', async (req, res) => {
	if (!req.body.name || !req.body.album) {
		return res.status(400).send({error: 'Data not valid'});
	}

	const trackData = {
		name: req.body.name,
		album: req.body.album,
		duration: req.body.duration || null
	}

	const track = new Track(trackData);

	try {
		await track.save();
		res.send(track);
	} catch {
		res.status(400).send({error: 'Data not valid'});
	}
});

module.exports = router;