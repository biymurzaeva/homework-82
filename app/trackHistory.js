const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
	const token = req.get('Authorization');

	if (!token) {
		return res.status(401).send({error: 'No token present'});
	}

	const user = await User.findOne({token});

	if (!user) {
		return res.status(401).send({error: 'Wrong token'});
	}

	const trackHistoryData = {
		user: user._id,
		track: req.body.track,
		datetime: new Date().toISOString()
	};

	const trackHistory = new TrackHistory(trackHistoryData);

	try {
		await trackHistory.save();
		res.send(trackHistory);
	} catch (e) {
		res.sendStatus(500);
	}
});

module.exports = router;