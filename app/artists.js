const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const Artist = require('../models/Artist');
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		const products = await Artist.find();
		res.send(products);
	} catch {
		res.sendStatus(500);
	}
});

router.post('/', upload.single('image'), async (req, res) => {
	if (!req.body.name) {
		return res.status(400).send({error: 'Data not valid'});
	}

	const artistData = {
		name: req.body.name
	};

	if (req.file) {
		artistData.image = req.file.filename;
	}

	if (req.body.info) {
		artistData.info = req.body.info;
	}

	const artist = new Artist(artistData);

	try {
		await artist.save();
		res.send(artist);
	} catch {
		res.status(400).send({error: 'Data not valid'});
	}
});

module.exports = router;